# 42 / ft_linear_regression

## Requirements
- `pipenv`

## Quickstart
- `pipenv install`

- `pipenv shell`

- `python train.py`

- `python train.py -h`

- `python predict.py`
