import json
import numpy as np
import matplotlib.pyplot as plt
import argparse
import time

def get_data_from_csv():
    return np.delete(np.genfromtxt('data.csv', delimiter=","), 0, 0)

def model(X, theta):
    return X.dot(theta)

def cost_function(X, y, theta):
    m = len(y)
    return (1 / 2 * m) * np.sum((model(X, theta) - y) ** 2)

def grad(X, y, theta):
    m = len(y)
    return 1 / m * X.T.dot(model(X, theta) - y)

def gradient_descent(X, y, learning_rate, n_iterations):
    theta = np.random.randn(2, 1)
    cost_history = np.array([0] * n_iterations, float)
    for i in range(0, n_iterations):
        theta = theta - learning_rate * grad(X, y, theta)
        cost_history[i] = cost_function(X, y, theta)
    return theta, cost_history

def write_output_to_json(theta_final, xmin, xmax):
    tf = theta_final.tolist()
    json_object = json.dumps({
        "theta0": tf[1][0],
        "theta1": tf[0][0],
        "xmin": xmin,
        "xmax": xmax,
    }, indent = 4)
    with open("data.json", "w") as outfile:
        outfile.write(json_object)

def display(x, y, X, theta_final):
    plt.scatter(x, y)
    plt.plot(x, model(X, theta_final), c='r')
    plt.show()

def cost_history_curve(i, cost_history) :
	plt.plot(range(i), cost_history)
	plt.show()

def coef_determination(y, pred):
    u = ((y - pred) ** 2).sum()
    v = ((y - y.mean()) ** 2).sum()
    return 1 - u / v

def train(n_iterations):
    data = get_data_from_csv()
    x = data[:, 0]
    kms = np.copy(x)
    xmin = np.min(x)
    xmax = np.max(x)
    for i in range(len(x)):
        x[i] = (x[i] - xmin) / (xmax - xmin)
    x = x.reshape(x.shape[0], 1)
    y = data[:, 1]
    y = y.reshape(y.shape[0], 1)
    X = np.hstack((x, np.ones(x.shape)))

    theta_final, cost_history = gradient_descent(X, y, 0.05, n_iterations)
    write_output_to_json(theta_final, xmin, xmax)
    return kms, y, X, theta_final, cost_history

def parse_args() :
	parser = argparse.ArgumentParser()
	parser.add_argument("-n", "--n_iterations", help="Change n_iterations value", type=int, default=10000)
	parser.add_argument("-d", "--display", help="Display a graph with all the data", action="store_true")
	parser.add_argument("-c", "--cost_history", help="show the cost history curve", action="store_true")
	return parser.parse_args()

try:
    args = parse_args()
    start_time = time.time()
    x, y, X, theta_final, cost_history = train(args.n_iterations)
    exec_duration = time.time() - start_time
    coef_determination = coef_determination(y, model(X, theta_final))
    print("n_iterations = " + str(args.n_iterations))
    print("r2 = " + str(coef_determination))
    print("exec_duration = " + str(int(exec_duration * 1000)) + " ms")
    if args.display:
        display(x, y, X, theta_final)
    elif args.cost_history:
        cost_history_curve(args.n_iterations, cost_history)
except Exception as e:
    print("[Error] " + str(e))
    exit()
