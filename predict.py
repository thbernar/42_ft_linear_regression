import json

def get_values_from_json():
    with open('data.json') as json_file:
        data = json.load(json_file)
    return data['theta0'], data['theta1'], data['xmin'], data['xmax']

def main():
    theta0, theta1, xmin, xmax = get_values_from_json()
    km = input("Quel est le kilométrage de votre voiture ? ")
    if theta0 == 0 and theta1 == 0 or xmax - xmin == 0:
        price = 0
    else:
        km_normalized = (float(km) - xmin) / (xmax - xmin)
        price = theta0 + km_normalized * theta1
    print("Votre voiture est estimée à " + str(price) + " euros.")

try:
    main()
except Exception as e:
    print("[Error] " + str(e))
    exit()
